import cv2
import os
import numpy as np
from PIL import Image

# face_xml_path = os.path.join('.','haarcascade_frontalface_default.xml')
# detector = cv2.CascadeClassifier(face_xml_path)
detector = cv2.CascadeClassifier(r'..\..\resources\haarcascade_frontalface_default.xml')
# detector = cv2.CascadeClassifier(r'D:\Python\pycharm\Face_Recognition\haarcascade_frontalface_alt2.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()


def get_image_label(path):
    image_paths = [os.path.join(path,f) for f in os.listdir(path)]
    face_samples = []
    ids=[]

    for image_path in image_paths:
        image=Image.open(image_path).convert('L')
        image_np = np.array(image,'uint8')
        if os.path.split(image_path)[-1].split(".")[-1]!='jpg':
            continue
        image_id=int(os.path.split(image_path)[-1].split(".")[1])
        faces=detector.detectMultiScale(image_np)
        for(x,y,w,h) in faces:
            face_samples.append(image_np[y:y+h,x:x+w])
            ids.append(image_id)
    return face_samples,ids

print('正在训练数据，请稍等...')
# train_data_path=os.path.join('.','train_data','train.yml')
# face_data_path=os.path.join('.','face_pic')

train_data_path='..\\train_model\\train.yml'
face_data_path='..\\Face_Data'
faces,Ids = get_image_label(face_data_path)
recognizer.train(faces,np.array(Ids))
recognizer.save(train_data_path)
print("{0}的数据集已经训练完成".format(len(np.unique(Ids))))
