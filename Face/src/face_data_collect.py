import cv2
import os

def face_collect():
    cap = cv2.VideoCapture(0)
    # face_xml_path = os.path.join('.','haarcascade_frontalface_default.xml')
    # face_detector = cv2.CascadeClassifier(face_xml_path)
    face_detector = cv2.CascadeClassifier(r'..\resources\haarcascade_frontalface_default.xml')
    # face_detector = cv2.CascadeClassifier(r'D:\Python\pycharm\Face_Recognition\haarcascade_frontalface_alt2.xml')
    face_id = input('\n 标签值')
    print('\n 正在执行，请稍等...')

    pic_count = 0

    while True:
        ret,img=cap.read()
        gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        faces = face_detector.detectMultiScale(gray,1.3,5)

        for (x,y,w,h) in faces:
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            pic_count = pic_count+1
            # save_path = os.path.join('.','face_collect','user%s.%s.jpg')
            # cv2.imwrite(save_path%(face_id,pic_count),gray[y:y+h,x:x+w])
            cv2.imwrite("../Face_Data/user."+str(face_id)+'.'+str(pic_count)+'.jpg',gray[y:y+h,x:x+w])
            cv2.imshow('Face Frame',img)

        k = cv2.waitKey(100)
        if k==27:
            break
        elif pic_count>=50:
            break;

    cap.release()
    cv2.destroyAllWindows()

face_collect()