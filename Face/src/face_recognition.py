import cv2
import numpy as np
import os

recognizer=cv2.face.LBPHFaceRecognizer_create()
# train_data_path=os.path.join('.','train_data','train.yml')
train_data_path='..\\train_model\\train.yml'
recognizer.read(train_data_path)

# face_xml_path=os.path.join('.','haarcascade_frontalface_default.xml')
face_cascade=cv2.CascadeClassifier(r'..\resources\haarcascade_frontalface_default.xml')
# face_cascade=cv2.CascadeClassifier(r'D:\Python\pycharm\Face_Recognition\haarcascade_frontalface_alt2.xml')
cap=cv2.VideoCapture(0)
font=cv2.FONT_HERSHEY_SIMPLEX

names=['AAA','ABC']

while True:
    ret,img=cap.read()
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    faces=face_cascade.detectMultiScale(gray,1.2,5)
    for(x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
        img_id,conf=recognizer.predict(gray[y:y+h,x:x+w])
        if conf<50:
            if img_id==1:
                img_id = "KingselyCen"
        else:
            img_id="Unknown"
        cv2.putText(img,str(img_id),(x+25,y-15),font,0.55,(255,0,0),2)
    cv2.imshow('Image',img)
    k=cv2.waitKey(10)
    if k==27:
        break
cap.release()
cv2.destroyAllWindows()