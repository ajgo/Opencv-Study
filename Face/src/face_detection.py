import numpy as np
import cv2
import os

def face_detect():
    # 加载人脸分类器
    # face_xml_path = os.path.join('.','haarcascade_frontalface_default.xml')
    # faceCascade = cv2.CascadeClassifier(face_xml_path)

    faceCascade=cv2.CascadeClassifier(r'..\resources\haarcascade_frontalface_default.xml')
    # faceCascade=cv2.CascadeClassifier(r'D:\Python\pycharm\Face_Recognition\haarcascade_frontalface_alt2.xml')
    cap = cv2.VideoCapture(0)
    isOpen = True

    while isOpen:
        isOpen, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(gray,1.3,5)

        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w,y + h), (255, 0, 0), 2)
        cv2.imshow('Video', img)

        k = cv2.waitKey(1)
        if k == 27:
            break;
    cap.release()
    cv2.destroyAllWindows()

face_detect()
