from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *

from open_video import open_video


def handle_open_video():
    open_video()


class Main:
    def __init__(self):
        file_main = QFile('./ui/main.ui')
        file_main.open(QFile.ReadOnly)
        file_main.close()
        self.window = QUiLoader().load(file_main)
        self.window.button.clicked.connect(handle_open_video)


if __name__ == '__main__':
    app = QApplication()
    main = Main()
    main.window.show()
    app.exec_()
