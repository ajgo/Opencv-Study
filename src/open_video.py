import cv2


def open_video():
    capture = cv2.VideoCapture(0)
    fps = 20
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    sz = (int(capture.get(cv2.CAP_PROP_FRAME_WIDTH)),
          int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    write = cv2.VideoWriter()
    write.open('../output/test.mp4', fourcc, fps, sz, True)
    count = 0
    while count < 1:
        count += 1
        _, frame = capture.read()
        cv2.imwrite(f'../output/{count}.jpg', frame)
        write.write(frame)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('frame', gray)
        if cv2.waitKey(1) == ord('q'):
            capture.release()
            write.release()
            cv2.destroyWindow('frame')
            break
